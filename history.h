//history.h

//Project 3: mysh Custom Shell

//Defines all the code to be used in the main file dealing with the history.
//Also defines the history struct, which will be used when dealing with the history.
//Aside from bloating files, it was a clear choice to move all of this out of main, 
//because it was an entirely different thing. 

//@author Andrew Baumher

#ifndef HISTORY_H
#define HISTORY_H

#include <stdio.h>

//a circular buffer. it is made using an array (dynamically allocated, but only to the max_size
typedef struct histStruct
{
    char ** history;
    int nElems;
    int first;//location of the first element in the circular set. used for the updateStruct method.
}histStruct;

//creates the history and makes sure nothing is uninitialized.
//takes the number of elements to store in history. 
void createHistory(unsigned int maxLen);

//returns the string storing information about a previous call.
//takes the user input number and the current iteration of the shell
//returns the element at the given number, or NULL, if it is out of range. 
char * getBang(int num, int current);

//adds the element to the history struct, allocating new space as needed or overwriting
//other elements as they fall off the list.
//takes the arg count and arg array, which will be parsed and added.
void updateHistory(int argc, char ** argv);

//prints the history. takes the current iteration of the loop. uses it so history numbers
//and commands match up.
void printHistory(int current);

//cleanup
void destructHistory();

#endif
