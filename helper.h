//helper.h

//Project 3: mysh Custom Shell

//Defines all the custom code to be used in the main file. they need not be defined here,
//but this allows me to cut down on bloated files. 

//@author Andrew Baumher

#ifndef HELPER_H
#define HELPER_H

//include statements
#include <stdio.h>
//custom defined getLine. the source of reference is credited in the .c file
//i cannot stress enough that the example was actually broken, and i had to make it my own
//so it would actually work the way the original author intended.
//takes the file pointer to the file to read from. 
//returns the dynamically allocated string read from the file. 
char * getLine(FILE * fp);

//parses a string into tokens, based on custom specifications.
//takes a string, and splits it into tokens.
//returns the allocated token array
char ** parseStr(char * lin);

//combines the previous two functions, but also cleans up the line returned by getline.
//required separately, because freeing a string obtained via the getBang would cause double
//frees and the history wouldn't work properly. 
char ** makeArgs(FILE * fp);

//cleans up the token array. 
void delArgs(char ** args);
#endif
