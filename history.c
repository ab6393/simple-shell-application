//history.c

//Project 3: mysh Custom Shell

//implements the history.h file

//Deals with the history structure and storing the history so it can be shown to the user
//and so it can return information in the case of the bang. functions are responsible for
//initializing, maintaining, and then destructing the history. 

//@author Andrew Baumher

//include statements.

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "history.h"

//in hindsight, could have put this in the history struct.
//at the time, it was set up so you could reinit history so as to restart it,
//which would mean hist_size would be lost. i didn;t need to do this, so it was a bit
//silly in retrospect.
static unsigned int HIST_SIZE;

//static struct for the history.
static histStruct * historyStruct = NULL;
//

//allocates the history and makes sure nothing is uninitialized.
void createHistory(unsigned int maxLen)
{
	HIST_SIZE = maxLen;
	historyStruct = malloc(sizeof(histStruct));
	historyStruct->history = NULL;
	historyStruct->first = 0;
	historyStruct->nElems = 0;
}

char * getBang(int num, int current)
{
	//can't run a bang on the current command, so also equals.
	//It can't bang itself :p.
	//Also checks to see if the number is out of the range of history on the bottom.
	if (num >= current || num < (current - historyStruct->nElems))
	{
		return NULL;
	}	
	//otherwise print it (just like bash does). and return it.
	else 
	{
		printf("%s\n", historyStruct->history[(num-1) % HIST_SIZE]);
		return (historyStruct->history[(num-1) % HIST_SIZE]);
	}
}
//adds items to history. does so dynamically, up until max size is reached. then it acts
//like a circular buffer.
void updateHistory(int argc, char ** argv)
{
	//allocate space for string, starting with the first argument.
	int size = strlen(argv[0]) + 1;
	char * newItem = malloc(sizeof(char) * size);
	//copy it over.
	strncpy(newItem, argv[0], size-1);
	newItem[size-1] = '\0';
	//repeat for all tokens. 
	for (int x = 1; x < argc; x++)
	{
		size += strlen(argv[x]) + 1;
		char * temp = realloc(newItem, sizeof(char) * size);
		newItem = temp;
		//i like sprintf, so i used this. i guess i could have used strncat, but i would
		//have needed multiple calls to append a ' ' character. 
		sprintf(newItem,"%s %s" ,newItem, argv[x]);
	}

	//if not the max size yet, dynamically allocate via realloc one additional size.
	if (historyStruct->nElems < (int) HIST_SIZE)
	{
		char ** temp = realloc(historyStruct->history, sizeof(char *) * ++(historyStruct->nElems));
		//then add it
		historyStruct->history = temp;
		historyStruct->history[historyStruct->nElems - 1] = newItem;
		historyStruct->history[historyStruct->nElems - 1][strlen(newItem)] = '\0'; 
	}
	//otherwise, free the first item in circular buffer, set it to the new one, and update
	//the first int so it represents the new first. 	
	else
	{
		//if on 11 in main, given HIST_SIZE = 10, then first is 0.
		free(historyStruct->history[historyStruct->first]);
		historyStruct->history[historyStruct->first] = newItem;
		historyStruct->history[historyStruct->first][strlen(newItem)] = '\0'; 
		historyStruct->first = (historyStruct->first + 1) % HIST_SIZE;
		//the eleventh is written at 0, first is 1. 
	}
}

void printHistory(int current)
{
	//ex, if HIST_SIZE is 10 and history was called on 12 - current
	//offset is 3. first = 2, which is the third elemet in the array. 
	//will print 3 through 12.  
	int	offset = current - historyStruct->nElems + 1;
	for (int x = 0; x < historyStruct->nElems; x++)
	{
		printf("\t%d: %s\n", x + offset, historyStruct->history[historyStruct->first]);
		historyStruct->first = (historyStruct->first + 1) % historyStruct->nElems;
	} 

}

void destructHistory()
{
	if (historyStruct != NULL)
	{
		for (int x = 0; x< historyStruct->nElems; x++)
		{
			free(historyStruct->history[x]);
		}
		free(historyStruct->history);
		free (historyStruct);
	}	
}
