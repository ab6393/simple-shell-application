//helper.c

//Project 3: mysh Custom Shell

//implements the helper.h file.

//defines custom functions that will be used in the mysh main file. these helpers have been
//separated so as to cut down on bloated mysh files. Originally contained history, which 
//has since been broken off further. 

//@author Andrew Baumher

//include statements

//printf, etc.
#include <stdio.h>
//NULL, etc.
#include <stdlib.h>
//string related functions top (strlen)
#include <string.h>

#include "helper.h"
//Cleanup on exit conditions. If careful may be able to remove this. 
#include "history.h"

//NOTE: This is a custom, cross-platform version of the GNU getline() function.
//I found this online, from the following site.
// https://sucs.org/Knowledge/Help/Program%20Advisory/Reading%20an%20arbitrarily%20long%20line%20in%20C
//SUCS provides Free Open Source Software (FOSS). The file was provided for public use, 
//But putting this in as a disclaimer nonetheless.
//Also worth noting is that i do not believe a single line has veen left unaltered. 
//Much had to be done to make it actually work as intended.  

//BUFSIZ is a whopping 8192 bytes. which is awesome, but odds are a single line is not 8192
//characters long. The original used BUFSIZ as the size to realloc, but "optimal read size"
//is hardly worth the jump in size. All internal commands are less than 16 bytes
#define PSEUDO_BUFSIZ 32

char * getLine(FILE * fp)
{
	size_t size = 0;
    size_t len  = 0;
    size_t last = 0;
    char * buf  = NULL;
	if(feof(fp))
	{
		return NULL;	
	}
    do {
		size+= PSEUDO_BUFSIZ;
        char * temp = realloc(buf,size); // realloc acts as a malloc when buf is null.             
		buf = temp;
        //Get BUFSIZE characters, and writes them to the address at the end of buf.
		//Since last is the position of the '\0' character, it overwrites this in subsequent
		//calls, and puts it at the end. Note that it doesn't actually erase it, so when
		//it exits the loop, it will still work.
		char * read = fgets(buf+len,PSEUDO_BUFSIZ,fp);
		//only occurs if EOF is the first character read via fgets.
		if (read !=NULL)
		{
        	len = strlen(buf);
        	last = len - 1;
		}
		else 
		{
			//if the first character read at all is the EOF, free buf and return NULL
			if (len == 0)
			{
				free(buf);
				return NULL;
			}
			//otherwise, we will exit the while loop (feof is true)
			//this way, we can still have the rare edge case where an EOF is read partway
			//through a getline, and at the first position of a subsiquent fgets.
			//using PSEUDO_BUFSIZ, this would occur if a file had 31 bytes on a line,
			//immediately followed by an EOF character. 
		}
    } while (!feof(fp) && buf[last]!='\n');

	//Note that this alters the idea of getline so as to not have the newline. 
	//it is understandable why they did it that way, but it does not help me.
	if (buf[last] == '\n')
	{
		buf[last--] = '\0';
	}
    if (sizeof(buf) - 1 > strlen(buf))
	{
		//fgets puts the '\0' at the end. 
		//free unneeded memory.
		char * temp = realloc(buf,strlen(buf) + 1);
		buf = temp;
	}
	return buf;
}

char ** parseStr(char * lin)
{
	//in the event the line obtained is null, return null
	if (lin == NULL)
	{
		return NULL;	
	}
	char ** strArr = NULL;
	char * str;
	//stores the string passed in so it can be edited without fear of editing the original
	char line[strlen(lin) + 1];
	strncpy(line, lin, strlen(lin) + 1);

	//loops through, replacing all spaces with ' character, unless it hits a quote.
	//then it replaces it with a single quote, and does not change anything until reaching
	//another quote. quotes must match: i.e. double to double, single to single. 
	//this way i can call strtok with the delims '\'' and still keep my spaxes in quotes.
    int arrSize = 0;
    int lineSize = strlen(line);
	for (int x = 0; x < lineSize; x++)
    {
        if (line[x] == ' ')
        {
			line[x] = '\'';
        }
        else if (line[x] =='\'')
        {
			x++;
            while (x != lineSize)
            {
                if (line[x] == '\'')
                {
                    break;
                }
                x++;
            }
            if (x == lineSize)
            {
				//checks for missing end quote, returning null if hit.
				//Note that this means it will cause the code to continue without increment
				//again, unspecified. 
                fprintf(stderr, "Missing ending \' character\n");
                return NULL;
            }
		}
        else if (line[x] =='\"')
        {
            line[x] = '\'';
            while (x != lineSize)
            {
                if (line[x] == '\"')
                {
                    line[x] = '\'';
                    break;
                }
                x++;
            }
            if (x == lineSize)
            {
				//checks for missing end quote, returning null if hit.
				//Note that this means it will cause the code to continue without increment
				//again, unspecified. 
                fprintf(stderr, "Missing ending \" character\n");
				return NULL;
            }
        }
    }
	str = strtok(line, "\'");
	//loops, adding a new token each subsiquent call.
	while (str != NULL)
	{
		//each token returned is added to a dynamically allocated string array.
		char ** tempStr = realloc(strArr, sizeof(char *) * (++arrSize));
		strArr = tempStr;
		//each string is then allocated and copied
		strArr[arrSize-1] = malloc(sizeof(char) * (strlen(str) + 1));
		strncpy(strArr[arrSize-1], str, strlen(str) + 1);
		str = strtok(NULL, "\'");
	}
	//adds another char * at the end, which is set to NULL. 
	//this way the size can be determined later on.
	char ** temp = realloc(strArr, sizeof(char *) * (arrSize + 1));
	strArr = temp;
	strArr[arrSize] = NULL;
	return strArr;
}

//allows memory management to be done in the background. and prevents double frees.
char ** makeArgs(FILE * fp)
{
	char * line = getLine(fp);
	char ** args = parseStr(line);
	free (line); 
	return args;
}

void delArgs(char ** args)
{
	int x = 0;
	while(args[x] != NULL)
	{
		free(args[x++]);
	}	
	free(args);
}
