//mysh.c

//Project 3: mysh Custom Shell

//Defines all the code required to be in the main. also has the main method, which runs
//everything. This code will take user commands and run them, just as a regular shell
//would. it also has a few internal commands it runs as well. stores a count that 
//does not increment on specific occasions. calls functions in history and helper to run. 

//@author Andrew Baumher

//include statements.
#include "history.h"
#include "helper.h"
#include "mysh.h"

//Stdio included vi other header files.
//bool data type.
#include <stdbool.h>
//NULL, etc.
#include <stdlib.h>
//Strtok, strncpy, etc.
#include <string.h>
//UINT_MAX
#include <limits.h>
//Error checking
#include <errno.h>
//pid_t
#include <sys/types.h>
//WIFEXITED
#include <sys/wait.h>
//fork, execvp
#include <unistd.h>
#define DEFAULT_HISTORY_SIZE 10

//needed for use with history and execute, but couldn't pass them. thus, global.
static int count = 1;
static bool verbose = false;

int mysh_help(int argc, char * argv[])
{
	//Useless
	(void) argc;
	(void) argv;
	printf("mysh custom shell\nInternal Commands:\n");
	printf("!N\t\t Repeat the Nth command. N must be in the range of history\n");
	printf("help\t\t Displays more information about internal commands\n");
	printf("history\t\t Displays the last N number of commands run, with their arguments.\n");
	printf("\t\t Defaults to 10. Set with the -h pos_num flag when mysh is run.\n");
	printf("quit\t\t Exits the mysh shell\n");
	printf("verbose [on|off] Turns verbose debug mode to given argument. toggles it if none provided.\n");
	return EXIT_SUCCESS;
}

//exec, fork, wait done here. 
int execute (int argc, char * argv[])
{
	pid_t pid;
	int status;
	int y = 0;
	
	//if fork fails, 
	if ((pid = fork()) < 0)
	{
		//cleanup and exit
		destructHistory();
		delArgs(argv);
		perror("Fork: ");
		exit(EXIT_FAILURE);	
	}
	//else if the child
	else if (pid == 0)
	{
		if (verbose)
		{
			//output the info about the command. 
			printf("\tCommand:");
			for(int x = 0; x< argc; x++)
			{
				printf(" %s", argv[x]);
			}
			//print tokens
			printf("\n\n\t Input Command Tokens\n");
			for (int x = 0; x < argc; x++)
			{
				printf("\t%d: %s\n", x, argv[x]);
			}
			printf("\twait for pid %d: %s", pid, argv[0]);
			printf("\texecvp: %s", argv[0]);
		}
		//execute the line. execvp takes control, and returns a value. 
		if ( execvp(argv[0], argv) < 0)
		{
			//if it fails, display error messages and e
			if (verbose)
			{
				while(argv[y])
				{
					fprintf(stderr, "%s: ", argv[y++]);
				}
				fprintf(stderr, "%s\n", strerror(errno));
			}
		}
		exit(0);
	}
	//otherwise we are the parent.
	else 
	{
		//ppid may not be the best name, but it makes sense to me, as we are checking for
		//the parent process id. 
		pid_t ppid;
		do 
		{
			//wait for the specific id matching the child. 
			ppid = wait(&status);
			//if wait fails, cleanup and exit.
			if (ppid == -1)
			{
				perror("wait");
				delArgs(argv);
				destructHistory();
				exit(EXIT_FAILURE);	
			}
		}
		while(pid != ppid);
		if (verbose)
		{		
			//we are required to deal with signal's not just regular exits. but how this is
			//to be done is unspecified. the man page shows how this is done; so yes, these
			//snippets are taken from there. however, implementation of this is really hard
			//to do any other way, as well, this is the most sound approach.
			if (WIFEXITED(status)) 
			{
				printf("command status: %d\n", WEXITSTATUS(status));
			}
			else if (WIFSIGNALED(status)) 
			{
				printf("killed by signal %d\n", WTERMSIG(status));
			} 
			else if (WIFSTOPPED(status)) 
			{
			printf("stopped by signal %d\n", WSTOPSIG(status));
			} 
			else if (WIFCONTINUED(status)) 
			{
				printf("continued\n");
			}
		}
	}
	return (int) pid;
}

int mysh_verbose(int argc, char * argv[])
{
	//if only one arg, toggle.
	if (argc < 2)
	{
		verbose = !verbose;	
	}
	//otherwise check and see if it says on or off.
	else 
	{
		if (strcmp(argv[1], "on") == 0)
		{
			verbose = true;
		}
		else if (strcmp(argv[1], "off") == 0)
		{
			verbose = false;
		}
		//if neither return exit failure. verbose remains unchanged.
		else
		{ 
			fprintf(stderr, "Usage: verbose [on|off]\n");
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}

int mysh_quit(int argc, char * argv[])
{
	(void) argc;
	(void) argv;
	destructHistory();
	return EXIT_SUCCESS;	
}

int mysh_history(int argc, char * argv[])
{
	//useless
	(void) argc;
	(void) argv;
	//print history.
	printHistory(count);
	return EXIT_SUCCESS;
}

//helper function used to parse the command line args -v flag
bool checkV(char * arg)
{
	if (strcmp(arg, "-v") ==0) return true;
	else return false;
}
//helper function to parse the command line args -h flag
bool checkH(char * arg1, char * arg2)
{
	//if the first arg is not -h
	if (strcmp(arg1, "-h") != 0)
	{
		return false;	
	}
	//if the second arg is not a number or is invalid. 
	char * temp = arg2;
	char * testParse;
	long count = strtol(temp, &testParse, 10);
	//edge case. not sure if needed, but oh well. 
	if ( count > UINT_MAX)
	{
		fprintf(stderr, "Value of history range is too large.\n");
		exit(EXIT_FAILURE);
	}
	//if arg2 is not a number or if it was, but was less than 0, it fails.
	else if (temp == testParse || count <= 0)
	{
		return false;
	}	
	//if nothing fails, it is -h.
	else
	{
		return true;
	}
	
}

int main(int argc, char * argv[])
{
	//Command Line parsing. A pain in the neck, yes. 
	//I looked into getopt, but at the time decided that i didn't like it b/c it uses
	//unistd.h, which limits portability. evidently, exec and execvp use unistd.h,
	//so in hindsight, i may have preferred that. Oh well.
	if (argc > 4)
	{
		fprintf(stderr, "Usage: [-v] [-h pos_num]\n");
		exit(EXIT_FAILURE);
			
	}
	else if (argc > 3)
	{
		if (checkH(argv[1], argv[2]) && checkV(argv[3])) 
		{
			verbose = true;
			char * temp = argv[2];
			unsigned int value = (unsigned int) strtol (temp, NULL, 10);
			createHistory(value);
		}
		else if (checkH(argv[2], argv[3]) && checkV(argv[1]))
		{
			verbose = true;
			char * temp = argv[3];
			unsigned int value = (unsigned int) strtol (temp, NULL, 10);
			createHistory(value);

		}
		else
		{
			fprintf(stderr, "Usage: [-v] [-h pos_num]\n");
			exit(EXIT_FAILURE);	
		}
	}
	else if (argc > 2)
	{
		if (checkH(argv[1],argv[2]))
		{
			char * temp = argv[2];
			unsigned int value = (unsigned int) strtol (temp, NULL, 10);
			createHistory(value);
		}
		else
		{
			fprintf(stderr, "Usage: [-v] [-h pos_num]\n");
			exit(EXIT_FAILURE);	
		}
	}
	else if (argc > 1)
	{
		if (checkV(argv[1]))
		{	
			verbose = true;
			createHistory(DEFAULT_HISTORY_SIZE);
		}
		else
		{
			fprintf(stderr, "Usage: [-v] [-h pos_num]\n");
			exit(EXIT_FAILURE);	
		}	
	}
	else
	{
		createHistory(DEFAULT_HISTORY_SIZE);	
	}

	//main loop for execution. checks against end of file for stdin or whatever stdin is
	//redirected to.
	while(!feof(stdin))
	{
		printf("mysh[%d]> ", count);
		//get the arguments from the command line, and turn them into a pseudo argv
		char ** nargv = makeArgs(stdin);
		//if empty, continue the while loop.
		if (nargv == NULL)
		{
			continue;
		}
		//get the pseudo argc. note that makeargs adds a null to "terminate" the array.
		int nargc = 0;
		while (nargv[nargc] != NULL)
		{
			nargc++;
		}
		//if empty cleanup and continue while loop.
		if (nargc == 0)
		{
			delArgs(nargv);
			continue;
		}
		//otherwise, if the first character of the first token is !, get the command from 
		//history. then parse it.
		if(nargv[0][0] == '!')
		{
			//remove the !
			memmove(nargv[0], nargv[0]+1, strlen(nargv[0]));
			char * endptr;
			//attempt to parse as an int. 
			int histNum = strtol(nargv[0], &endptr, 10);
			//if the pointer set by strtol and endptr are the same it didn't parse anything
			//it still returns 0, so this is a better check.
			if (endptr == nargv[0])
			{
				//bad bangs are not defined. i have it continue and not increment. 
				delArgs(nargv);
				fprintf(stderr, "Usage: !num\n");	
				continue;
			}
			else
			{
				//otherwise get the line associated with the bang.
				char * line = getBang(histNum, count);
				//delete old pseudo argv, 
				delArgs(nargv);
				//again, bad bangs are undefined. i clear and continue.
				if(line == NULL)
				{
					fprintf(stderr, "Error: %d Out of range\n", histNum);
					continue;	
				}
				//upated pseudo argv with the one from bang. 
				nargv = parseStr(line);
				//reset and refind the pseudo argc.
				nargc = 0;
				while (nargv[nargc] != NULL)
				{
        			nargc++;
                }
			}
		}
        //parsing. checks for help, history, verbose, quit, adding them to the history
        //otherwise assumes you meant an executable call, and runs execute.
        //cleans up afterward.
        if(strcmp(nargv[0], "help") == 0)
        {
        	updateHistory(1, nargv);
            mysh_help(nargc, nargv);
            delArgs(nargv);
            count ++;
		}
        else if(strcmp(nargv[0], "history") == 0)
        {
        	updateHistory(1, nargv);
            mysh_history(nargc, nargv);
            delArgs(nargv);
            count ++;
		}
		else if(strcmp(nargv[0], "verbose") == 0)
        {
        	if (mysh_verbose(nargc, nargv) == EXIT_SUCCESS)
            {
            	if (nargc > 1)
                {
                	updateHistory(2, nargv);
                }
                else
                {
                	updateHistory(1, nargv);
                }
            }
			else updateHistory(nargc, nargv);
        	delArgs(nargv);
        	count++;
		}
		else if(strcmp(nargv[0], "quit") == 0)
        {
        	mysh_quit(nargc, nargv);
            delArgs(nargv);
            return EXIT_SUCCESS;
		}
        else
        {
			updateHistory(nargc, nargv);
            execute(nargc, nargv);
            delArgs(nargv);
            count ++;
		}
	}
    //only reached if FEOF is triggered.
    destructHistory();
    return EXIT_SUCCESS;
}


