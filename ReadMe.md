# Simple Shell App

**Target OS: Linux. Tested on Ubuntu, WSL Ubuntu**

This is a simple version of a Linux shell, built in C. The concept behind this project was to become familiar with Linux development, namely Fork and Exec/Execvp. process ids and errnos were also used. It supports logging and history. 

### History

Keeps a log of the last few comands given. The default size of **10**. Size can be set manually with **-h [new size]**. Users can ask for their history with the **history [last n elements]** command, or use a bang (**!**) to execute a previously issued command. This implementation of bang only supports a number, as this was the scope of the project, but a bang command (such as **!echo** ) would be relatively easy to implement. History is implemented via a circly linked list. The list is lazy, only allocating memory when the number of elements increases past the currently allocated amount. This leaves a smaller footprint at the cost of slightly longer runtime, until the max amout of elements is hit. Given that the idea of the project was a small-scale shell, lower footprint seemed the better choice. 

### Logging (Verbose)

The shell also allows verbose logging, which, if enabled, logs what the Shell is receiving and doing. Each fork will log if it succeeds, and what the new process ids (pids) are. Each command that is passed to Execvp will also display that it is doing so. logging can be enabled initially with the **-v** flag. Logging can be expressly enabled or disabled with **verbose [true/false]** or toggled with **verbose**.

## Creation
using the MakeFile attached, simply run make.
## Usage

```sh
$ mysh [-h pos_num] [-v]
```
**-h pos_num:** sets the number of elements in the history to pos_num
**-v:** enables verbose logging

This enables the shell. Use the shell like any other shell
```sh
mysh$ <Command>
```
### Internal Commands

**history [number]:** displays the last **number** elements of history. defaults to **10**.
**verbose [on/off]:** toggles verbose logging, or sets it **on** or **off**
**help:** displays help text about this shell.
**quit:** exits the shell
## Examples
The following are captured from WSL Ubuntu.  
With no arguments:  
![Example with no arguments](img/Run1.png)  
With a history of 5:  
![Example with a history length set](img/Run2.png)  
A few additional runs:  
![Example: first run with verbose and history of 5. third run was invalid](img/Run3.png)
