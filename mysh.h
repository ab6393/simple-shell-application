//mysh.h

//Project 3: mysh Custom Shell

//Defines all the code required in the main file. All functions follow the required form
//of int name(int argc, char * argv[]); many call helper functions, which helps make the
//more secure, and helps keep main relatively small (it is already too large for my liking)

//@author Andrew Baumher

#ifndef MYSH_H
#define MYSH_H

//prints out usefull information to the user about the internal mysh commands
//argc and argv are the commands entered in the shell. they are not used.
//returns based on success of the method. since it does not fail, returns 0
int mysh_help(int argc, char * argv[]);

//toggles the verbose option by default.
//verbose can be set to on or off via a flag, passed via argv.
//returns based on whether or not verbose did anything. if not, an error number.
int mysh_verbose(int argc, char * argv[]);

//prints out the history, showing only the last few elements. the number is specified by
//the user, or defaults to 10. 
//argc and argv are the commands entered in the shell. they are not used.
//returns based on success of the method. since it does not fail, returns 0
int mysh_history(int argc, char * argv[]);

//cleans up the history struct
//argc and argv are the commands entered in the shell. they are not used.
//returns based on success of the method. since it does not fail, returns 0
int mysh_quit(int argc, char * argv[]);

//runs fork, execvp, and wait. basically, it takes care of anything not internal.
//uses argc and argv for execvp.
//returns the process id for the the parent. 
int execute(int argc, char * argv[]);

#endif
